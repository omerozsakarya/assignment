import sys


def read_file(path_of_file):
    with open(path_of_file) as f:
        content = f.read()

    content = content.strip()
    return [int(k) for k in content.split(" ")]


def find_highest_sum(p_list, p_max_length):
    dataset_length = len(p_list)
    result_list = list()

    if dataset_length > 0:
        max_sum = p_list[0]

        for i in range(dataset_length):
            sub_list = list()
            sub_list.append(p_list[i])
            j = i + 1

            while j < len(p_list) and j - i < p_max_length:
                sub_list.append(p_list[j])
                if sum(sub_list) > max_sum:
                    result_list = sub_list.copy()
                    max_sum = sum(sub_list)
                j = j + 1

    return sum(result_list)


def main(p_input_file, p_max_length, p_calculation_type):
    input_file = p_input_file
    max_length = p_max_length
    calculation_type = p_calculation_type

    if calculation_type == "differences":
        max_length = max_length - 1

    raw_sensor_data = read_file(input_file)
    sensor_data = list()

    if calculation_type == "differences":
        for index in range(len(raw_sensor_data)-1):
            sensor_data.append(abs(raw_sensor_data[index]-raw_sensor_data[index+1]))
    elif calculation_type == "values":
        sensor_data = raw_sensor_data.copy()

    return find_highest_sum(sensor_data, max_length)


def test():
    assert main("data/input_1.txt", 9, "values") == 6
    assert main("data/input_1.txt", 4, "differences") == 16
    assert main("data/input_2.txt", 10, "values") == 27
    assert main("data/input_2.txt", 5, "differences") == 58
    assert main("data/input_3.txt", 30, "values") == 44
    assert main("data/input_3.txt", 10, "differences") == 40


test()
print(main(sys.argv[1], int(sys.argv[2]), sys.argv[3]))

